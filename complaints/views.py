from django.shortcuts import render

# Create your views here.
from rest_framework import request
from rest_framework.response import Response
from rest_framework.views import APIView

from complaints.services import TicketDataService

"""
Tickets Data
"""


class HandleTicketsView(APIView):
    def get(self):
        pass

    def post(self, requests, *args, **kwrgs):
        ticket_service = TicketDataService()
        req = request.data
        try:
            res = ticket_service.create_ticket(req)
            return Response("Successfully Created", status=200)

        except Exception:
            pass


"""

logs info. debug, error
Atomicity (atomic transaction)
Security (TOkenAuthentication)
Exception Handling (i.e case by cases)

3 tier Architecture flow (view, services, managers, proxies)

"""
