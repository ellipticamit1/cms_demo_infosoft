"""

conatins proxy models for all your transaction models
"""
from complaints.managers import TicketDataManager
from complaints.models.ticket import Ticket


class TicketDataProxy(Ticket):
    objects = TicketDataManager()
