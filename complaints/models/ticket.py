from django.db import models
from reversion import revisions as reversion
from api.alias import AliasField
from api.models import BaseModel

from masters.models.tickettype import TicketType
from masters.models.ticketstatus import TicketStatus


class Ticket(BaseModel):
    ticket_id = models.AutoField(db_column='TICKET_ID', primary_key=True)
    # Field name made lowercase.
    ticket_title = models.CharField(db_column='TICKET_TITLE', max_length=25)
    # Field name made lowercase.
    ticket_description = models.CharField(
        db_column='TICKET_DESCRIPTION', max_length=255)
    ticket_type = models.ForeignKey(
        TicketType, on_delete=models.CASCADE, db_column='TICKET_TYPE', related_name='TICKET_TYPE')
    ticket_status = models.ForeignKey(
        TicketStatus, on_delete=models.CASCADE, db_column='TICKET_STATUS', related_name='TICKET_STATUS')
    name = AliasField(db_column='TICKET_TITLE', blank=True, null=True)

    class UI_Meta:
        ui_specs = {
            "listview": [
                "this value"
            ],

            "formview": [
                {
                    "sectionlabel": "Ticket Master",
                    "cols": 2,
                    "colComponent": [
                        {
                            "label": "Ticket",
                            "decorator": "ticket",
                            "type": "textbox",
                            "required": "true",
                            "message": "Enter Ticket",
                            "id": "ticket_id",
                            "placeholder": "Please Enter Ticket",
                            "maxlength": 3,
                            "disabled": False,
                        },
                        # {
                        #     "label": "State Name",
                        #     "decorator": "state_name",
                        #     "type": "textbox",
                        #     "required": "true",
                        #     "message": "Enter State name",
                        #     "id": "state_name",
                        #     "placeholder": "Please enter State Name",
                        #     "disabled": False,
                        # },
                        #
                        {
                            "label": "Ticket Type",
                            "decorator": "ticket_type",
                            "type": "select",
                            "required": "true",
                            "message": "Select Ticket Type",
                            "validator": None,
                            "validateStatus": None,
                            "id": "ticket_type_id",
                            "placeholder": "Please Select Ticket Type ",
                            "option": "TicketType",
                            "linked": "yes",
                            "link_api": "TicketType",
                            "primary_key_field": "ticket_type_id",
                            "foreign_table_attribute_name": "name",
                            "disabled": False,
                        },
                        {
                            "label": "Ticket Status",
                            "decorator": "ticket_status",
                            "type": "select",
                            "required": "true",
                            "message": "Select Ticket Status",
                            "validator": None,
                            "validateStatus": None,
                            "id": "ticket_status_id",
                            "placeholder": "Please Select Ticket Status",
                            "option": "TicketStatus",
                            "linked": "yes",
                            "link_api": "TicketStatus",
                            "primary_key_field": "ticket_status_id",
                            "foreign_table_attribute_name": "name",
                            "disabled": False,
                        },
                        {
                            "label": "Is Active",
                            "decorator": "is_active",
                            "type": "radio",
                            "radioType": "group",
                            "id": "isActive",
                            "required": "true",
                            "message": "Please select the status",
                            "listed": "yes",
                            "list_data": [
                                {
                                    "True": "Yes"
                                },
                                {
                                    "False": "No"
                                },
                            ],
                            "link_api": "isactive",
                            "disabled": False,
                        },

                        {
                            "label": "Created By",
                            "decorator": "created_by",
                            "type": "textbox",
                            "required": "true",
                            "message": "Created By !",
                            "id": "created_by",
                            "placeholder": "This is Created By :",
                            "disabled": True
                        },

                        {
                            "label": "Is Deleted",
                            "decorator": "is_deleted",
                            "type": "radio",
                            "radioType": "group",
                            "id": "isDeleted",
                            "required": "true",
                            "message": "Please select the status",
                            "listed": "yes",
                            "list_data": [
                                {
                                    "True": "True"
                                },
                                {
                                    "False": "False"
                                },
                            ],
                            "link_api": "isdeleted"
                        },

                        {
                            "label": "Created Date ",
                            "decorator": "created_date",
                            "required": "true",
                            "message": "Created Date ",
                            "placeholder": "Select Date",
                            "type": "date",
                            "id": "created_date",
                            "dateFormatList": "DD-MM-YYYY HH:mm:ss",
                            "disabled": True
                        },
                        {
                            "label": "Updated By ",
                            "decorator": "last_updated_by",
                            "type": "textbox",
                            "required": "true",
                            "message": "Last Updated By !",
                            "id": "last_updated_by",
                            "placeholder": "This was last Updated By:",
                            "disabled": True
                        },
                        {
                            "label": "Last Updated : ",
                            "decorator": "last_updated_date",
                            "required": "true",
                            "message": "Last Updated Date",
                            "placeholder": "Select Date",
                            "type": "date",
                            "id": "last_updated_date",
                            "dateFormatList": "DD-MM-YYYY HH:mm:ss",
                            "readonly": "true",
                            "disabled": True
                        },
                    ]
                }

            ]
        }

    class Meta:
        db_table = 'MST_TICKET'
        app_label = "complaints"


reversion.register(Ticket)
