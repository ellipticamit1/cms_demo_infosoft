from django.contrib import admin

from complaints.models.ticket import  Ticket

# Register your models here.
admin.site.register(Ticket)
