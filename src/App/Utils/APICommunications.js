import getCookie from "./getCookie";
import {COMPLAINT_DATA_SERVER} from "../AppConfig";

/**
 * API Calls
 * @param {getCookie} : Cookie Data
 * @param {MASTER_DATA_SERVER} : Details of api call, host, port
 * @param {app} : Name of app to be passed while making api call
 * @param {entity} : Entity name to be passed while making api call
 * @param {id} : ID of element to be passed while making api call
 * @param {values} : Json data to be passed while making api call
 */

/* Type of request call */
export const METHOD_PUT = "PUT";
export const METHOD_POST = "POST";
export const METHOD_GET = "GET";
export const METHOD_PATCH = "PATCH";
export const METHOD_DELETE = "DELETE";
export const METHOD_OPTIONS = "OPTIONS";

/* Type of credentials include */
const CREDENTIALS_INCLUDE = "include";
export const STANDARD_METHOD_OPTIONS = {
    credentials: CREDENTIALS_INCLUDE,
};

/* Cache setting details */
const CACHE_SETTINGS = {
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
};

/* Core setting details */
const CORS_SETTINGS = {
    mode: "cors", // no-cors, cors, *same-origin
    credentials: "same-origin", // include, *same-origin, omit
};

/* Header details with Auth token  */
export let HEADER_JSON = {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: JSON.parse(localStorage.getItem("auth-token")),
    source: "workflow",
    req: "list",
};

/* Api call URL generation  */
export let COMPLAINT_DATA_SERVER_URL = ((SERVER) =>
    `http://${SERVER.SERVER_URL}:${SERVER.PORT}/${SERVER.MASTER_ROUTE}`)(
        COMPLAINT_DATA_SERVER
);

export const get_all_tickets_list = (app, entity) => (
    fetch(COMPLAINT_DATA_SERVER_URL + "/" + app + "." + entity + "/list?page=1", {
        ...STANDARD_METHOD_OPTIONS,
        ...CACHE_SETTINGS,
        ...CORS_SETTINGS,
        method: METHOD_GET,
        headers: {
            ...HEADER_JSON,
            "Approval-Authorization": localStorage.getItem("approval-authorization"),
            "X-CSRFToken": getCookie("csrftoken"),
        },
    })
);

export const get_tickets_list_view = (app, entity) => (
    fetch(COMPLAINT_DATA_SERVER_URL + "/" + app + "." + entity + "/?set=listview", {
        ...STANDARD_METHOD_OPTIONS,
        ...CACHE_SETTINGS,
        ...CORS_SETTINGS,
        method: METHOD_GET,
        headers: {
            ...HEADER_JSON,
            "Approval-Authorization": localStorage.getItem("approval-authorization"),
            "X-CSRFToken": getCookie("csrftoken"),
        },
    })
);



