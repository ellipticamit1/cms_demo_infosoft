import {message,notification} from "antd";

/**
 * Notifcation tags
 * @param {success} : Success popup
 * @param {error} : Error popup
 * @param {warning} : Warning popup
 * @param {NotificationMessage} : Notification message
 */

export const success = (response) => {
    message.success(response);
};

export const error = (response) => {
    message.error(response);
};

export const warning = (response) => {
    message.warning(response);
};

export const showNotificationMessage = (type, notificationTitle, notificationDescription) => {
    notification[type]({
        message: notificationTitle,
        description: notificationDescription,
    });
};