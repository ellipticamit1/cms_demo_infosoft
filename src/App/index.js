import React from 'react';
import MDMApp from 'swfrontend/MDMApp'
import {localStorageVariableName} from "swfrontend/AppConfigs";
import LoginPage from "swfrontend/LOGIN/LoginPage";
import {BrowserRouter} from "react-router-dom";
import getRoutes from "../UI/Routing/ScreenRouters";
 
const App = () => {
    return (
        localStorage.getItem(localStorageVariableName.authToken) == null ?
            <LoginPage/> :
            <BrowserRouter><MDMApp routes={getRoutes}/></BrowserRouter>
    )
};


export default App;
