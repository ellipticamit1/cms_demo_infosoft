export let COMPLAINT_DATA_SERVER = {
    SERVER_URL: process.env.REACT_APP_SERVER_URL, // REST API IP HOST
    PORT: process.env.REACT_APP_SERVER_PORT, // REST API IP PORT
    MASTER_ROUTE: process.env.REACT_APP_API_MASTER_ROUTE,
};

export let COMPLAINT_URL = `http://${COMPLAINT_DATA_SERVER.SERVER_URL}:${COMPLAINT_DATA_SERVER.PORT}/`;

/* Screen root notification details */
export let screenRootNotificationData = {
    messageType: "error",
    messageTitle: "API",
    message: "Cannot Make API connection"
};

export const APP_NAME = '';

export let gridViewConfig = {
    paginationPageSize: process.env.REACT_APP_DEFAULT_PAGINATION_SIZE
};


/* Route URL */
export let routeUrl = {"url": process.env.REACT_APP_PROJECT_ROUTE};