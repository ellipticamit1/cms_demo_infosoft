import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Dropdown, Input, Menu} from "antd";

const {Search} = Input;

class ListHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: ""
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  handleSearch(){
    const {onSearch} = this.props;
    const {searchValue} = this.state;
    onSearch(searchValue);
  }

  onSearchChange(e){
    const {value} = e.target;
    this.setState({
      searchValue: value,
    });
  }

  render() {
    const {addButton, addField, deleteButton, deleteField, excelExport} = this.props;
    const {searchValue} = this.state;
    const menu = (
      <Menu>
        <Menu.Item key="1" hidden={!deleteButton} onClick={deleteField}>
          Delete
        </Menu.Item>
        <Menu.Item key="2" onClick={excelExport}>
          Export
        </Menu.Item>
      </Menu>
    );

    return (
      <div className="panel__header">
        <div className="panel__header-top">
          <div className="search__section">
            <Search
              placeholder="Search in table"
              onSearch={this.handleSearch}
              value={searchValue}
              style={{float: "left", width: "65%"}}
              onChange={this.onSearchChange}
              enterButton
            />
          </div>
          <div className="action__section">
            <Button
              className="add_header"
              type="primary"
              onClick={addField}
              hidden={!addButton}
            >
              New
            </Button>
            <Dropdown className="drop_down_header" overlay={menu}>
              <Button className="drop_down_header">Action</Button>
            </Dropdown>
          </div>
        </div>
      </div>
    );
  }
}
ListHeader.propTypes = {
  addField: PropTypes.any,
  deleteField: PropTypes.any
};

export default ListHeader;
