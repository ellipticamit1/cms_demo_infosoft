import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Pagination} from "antd";

class Footer extends Component {
  render() {
    const {total} = this.props;
    return (
      <div className="panel__footer">
        <Pagination
          showQuickJumper
          defaultCurrent={1}
          defaultPageSize={15}
          showSizeChanger
          total={total}
          onChange={() => {}}
          onShowSizeChange={() => {}}
        />
      </div>
    );
  }
}

Footer.propTypes = {
  total: PropTypes.number.isRequired,
};

export default Footer;
