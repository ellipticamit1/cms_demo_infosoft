import React from "react";
import {Route} from "react-router-dom";

import Tickets from '../../Tickets';

/**
 * Custom component mapping
 * @param {component} : Component being mapped
 * @param {routers} : Creating route for a component specified with route link
 */
function getRoutes() {
    return ({
        "Tickets": {
            component:  Tickets,
            routers: [<Route path="/app/Tickets/" component={Tickets}/>],
        },
    })
}

export default getRoutes;