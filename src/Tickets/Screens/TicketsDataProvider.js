import {get_all_tickets_list,get_tickets_list_view} from '../../App/Utils/APICommunications';

export class TicketsDataProvider {

    async loadAllTickets(app, entity) {
        try {
            const response = await get_all_tickets_list(app, entity);
            if(response.ok) {
                const data = await response.json();
                return data;
            } else {
                throw Error();
            }
        } catch(error) {
            return error;
        }
    }

    async loadTicketsListView(app, entity) {
        try {
            const response = await get_tickets_list_view(app, entity);
            if(response.ok) {
                const data = await response.json();
                console.log("Data ==== ", data);
                return data;
            } else {
                throw Error();
            }

        } catch(error) {
            return error;
        }
    }

}