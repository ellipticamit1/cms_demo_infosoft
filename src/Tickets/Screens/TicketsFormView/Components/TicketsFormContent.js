import React, {Component} from 'react';

import TicketsFormHeader from './TicketsFormHeader';
import TicketsFormRenderer from './TicketsFormRenderer';

class TicketsFormContent extends Component {
    render() {
        const {sections} = this.props;
        return (
            <div className="col-md-12">
                <TicketsFormHeader />
                <TicketsFormRenderer sections={sections} />
            </div>
        );
    }
}

export default TicketsFormContent;