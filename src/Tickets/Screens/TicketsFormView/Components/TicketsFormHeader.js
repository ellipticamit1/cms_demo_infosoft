import React, {Component} from "react";
import {Button, Form} from "antd";

class TicketsFormHeader extends Component {

    render() {
        const {onBackPressed, showSaveButton, toShowBackButton,editDataAvailable, formRef} = this.props;
        return (
            <div className="panel__header">
                <div className="panel__header-top">
                    <div className="back__section">
                        <Button
                            hidden={showSaveButton ? !toShowBackButton : false}
                            onClick={() => onBackPressed()}
                            className="login-form-button"
                        >
                            Back
                        </Button>
                    </div>
                    <div className="action__section">
                        <Form.Item style={{float: "right"}}
                                hidden={showSaveButton ? !showSaveButton : false}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                            >
                                Save
                            </Button>
                        </Form.Item>
                        <Form.Item style={{float: "right"}}>
                            <Button
                                type="link"
                                hidden={editDataAvailable}
                                onClick={() => {
                                    formRef.current.resetFields();
                                }}
                                className="login-form-button"
                            >
                                Clear
                            </Button>
                        </Form.Item>
                    </div>
                </div>
            </div>
        );
    }
}

export default TicketsFormHeader;