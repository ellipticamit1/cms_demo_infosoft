import React, { Component } from 'react';

class TicketsFormRenderer extends Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div>
                {this.props.sections.map((subsection, id) => {
                    return (
                        <div className="row form-box" key={id} id={subsection.sectionlabel}>
                            <legend key={subsection.sectionlabel} className="form-legend">
                                {subsection.sectionlabel}
                            </legend>
                            {subsection.colComponent.map((element) => { 
                                return (
                                    <div
                                        id={element.id}
                                        className=""
                                        key={element.id}
                                    >
                                        
                                    </div>
                                )
                               
                            })}
                        </div>
                    )}
                )}
            </div>
        );
    }
}

export default TicketsFormRenderer;