import React, {Component} from 'react';

import TicketsFormContent from './Components/TicketsFormContent';

import './style.css';

const mockData = {
    sections: [{
        colComponent: [{
            decorator: "ticket_title",
            disabled: false,
            id: "ticket_title",
            label: "Ticket Title",
            maxlength: 3,
            message: "Enter Ticket Title",
            placeholder: "Please Enter Ticket Title",
            required: "true",
            type: "textbox"
        }],
        cols: 2,
        sectionlabel: 'Tickets'
    }]
}


class TicketsFormView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownData: null,
            editFormData: null,
            editDataAvailable: false,
            sections: [],
            routeName: this.props.location.pathname.substr(1).split("/")[1],
            routeState: this.props.location.pathname.substr(1).split("/")[2],
            loading: true,
            fieldPermissions: [],
            versionList: null,
            listView: null,
        }
    }

    componentDidMount() {
        this.setState({
            sections: mockData.sections
        })
    }

    render() {
        return (
            <TicketsFormContent 
                sections={this.state.sections}
            />
        );
    }
}

export default TicketsFormView;