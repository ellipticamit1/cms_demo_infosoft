import React, {Component} from "react";

import TicketListViewContent from "./Components/TicketListViewContent";
import {showNotificationMessage} from "../../../App/Utils/NotificationMessage";
import {
  notificationTitle,
  notificationMessage,
  showNotificationType,
  routeMode,
} from "../../TicketsUIConfig/TicketsUIMapping";
import {TicketsDataProvider} from "../TicketsDataProvider";

import "./style.css";

const mockData = {
  listView: [
    {
      checkboxSelection: true,
      field: "ticket_id",
      filter: null,
      headerCheckboxSelection: true,
      headerName: "Ticket ID",
      primarykeyfield: "ticket_id",
      type: "integer",
    },
    {
      field: "ticket_title",
      filter: null,
      headerName: "Ticket Title",
      type: "text",
    },
    {
      field: "ticket_description",
      filter: null,
      headerName: "Ticket Description",
      type: "text",
    },
    {
      field: "ticket_type.name",
      headerName: "Ticket Type",
      type: "foreignkey",
    },
    {
      field: "ticket_status.name",
      headerName: "Ticket Status",
      type: "foreignkey",
    },
  ],
};

class TicketsListView extends Component {
  constructor(props) {
    super(props);
    const {location} = this.props;
    this.state = {
      routeName: location.pathname.substr(1).split("/")[1],
      listView: null,
      defaultColDef: {
        sortable: true,
        filter: true,
        resizable: true,
      },
      rowData: null,
      rowSelection: "multiple",
      selectedRow: null,
      addButton: false,
      deleteButton: false,
      searchQuery: null,
      total: 0,
    };

    this.onGridReady = this.onGridReady.bind(this);
    this.editField = this.editField.bind(this);
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.addField = this.addField.bind(this);
    this.handleSearchApply = this.handleSearchApply.bind(this);
    this.fetchTicketData = this.fetchTicketData.bind(this);

  }

  /* Setting gridApi variable onGridReady to fetch selected roww data */
  onGridReady(params){
    this.gridApi = params.api;
  }

  /* Storing selected row data to selectedRow variable */
  onSelectionChanged(){
    const selectedRows = this.gridApi.getSelectedRows();
    this.setState(() => ({
      selectedRow: selectedRows,
    }));
  }

    /* Editing selected data on button click */
    editField(){
      const {selectedRow, listview, routeName} = this.state;
      const {history} = this.props;
      // Check if data is there or not
      if (selectedRow != null) {
        // If single data is selected redirecting route to form view with editing detatils
        if (selectedRow.length === 1) {
          const temp = listview[0].primarykeyfield;
          const id = selectedRow[0][temp];
          history.push({
            pathname: `${routeName  }/${  id}`,
          });
        } else {
          showNotificationMessage(
            showNotificationType.warning,
            notificationTitle.edit,
            notificationMessage.listMultipleEditDataSelected
          );
        }
      } else {
        showNotificationMessage(
          showNotificationType.error,
          notificationTitle.edit,
          notificationMessage.listEditDataNotSelected
        );
      }
    }

  /* Redirecting to form add view on button click */
  addField(){
    const {routeName} = this.state;
    const {history} = this.props;
    history.push({
      pathname: `${routeName  }/${  routeMode.new}`,
    });
  }

  /* Search Appyly ond ata */
  handleSearchApply(value){
    this.gridApi.showLoadingOverlay();
    let queryParam = `search=${  value}`;
    if (value === "") {
      queryParam = null;
    }
    this.setState({searchQuery: queryParam}, () => this.fetchGridData(1));
  }

  async fetchTicketData(){
    try {
      const getticketData = new TicketsDataProvider();
      const {results, total} = await getticketData.loadAllTickets(
        "complaints",
        "Ticket"
      );
      this.setState({
        rowData: results,
        total,
      });
    } catch (err) {
      showNotificationMessage(
        showNotificationType.error,
        notificationTitle.api,
        notificationMessage.apiConnectionError
      );
    }
  }

  componentDidMount() {
    this.fetchTicketData();
    this.setState({
      listView: mockData.listView,
      rowData: mockData.rowData,
      addButton: true,
      deleteButton: true,
    });
  }

  render() {
    const {
      addButton,
      defaultColDef,
      deleteButton,
      rowData,
      listView,
      total,
      searchQuery,
      rowSelection
    } = this.state;

    return (
      <TicketListViewContent
        addField={this.addField}
        addButton={addButton}
        defaultColDef={defaultColDef}
        deleteButton={deleteButton}
        listView={listView}
        rowData={rowData}
        total={total}
        onGridReady={this.onGridReady}
        rowDoubleClicked={this.editField}
        onSelectionChanged={this.onSelectionChanged}
        onSearch={this.handleSearchApply}
        searchQuery={searchQuery}
        rowSelection={rowSelection}
      />
    );
  }
}

export default TicketsListView;
