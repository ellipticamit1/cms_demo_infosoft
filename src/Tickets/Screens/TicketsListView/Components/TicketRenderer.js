import React, {Component} from "react";
import PropTypes from "prop-types";
import {AgGridReact} from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import {gridViewConfig} from "../../../../App/AppConfig";

class TicketRenderer extends Component {

  render() {
    const {
      defaultColDef,
      listView,
      rowData,
      rowSelection,
      rowDoubleClicked,
      onGridReady,
      onSelectionChanged,
    } = this.props;

    return (
      <div
        className="table-responsive table-bordered 
                table table-hover table-bordered results
                ag-theme-balham gridViewDisplay"
      >
        <AgGridReact
          animateRows
          columnDefs={listView}
          defaultColDef={defaultColDef}
          onGridReady={onGridReady}
          onRowDoubleClicked={rowDoubleClicked}
          onSelectionChanged={onSelectionChanged}
          pagination={false}
          paginationPageSize={gridViewConfig.paginationPageSize}
          rowData={rowData}
          rowSelection={rowSelection}
          suppressRowClickSelection={false}
        />
      </div>
    );
  }
}

TicketRenderer.propTypes = {
  listView: PropTypes.any,
  defaultColDef: PropTypes.any,
  rowData: PropTypes.any,
  onGridReady: PropTypes.func,
  rowSelection: PropTypes.any,
  onSelectionChanged: PropTypes.func,
  rowDoubleClicked: PropTypes.func,
};

export default TicketRenderer;
