import React, {Component} from "react";

import {ListHeader} from "../../../../UI/Header";
import TicketRenderer from "./TicketRenderer";
import Footer from "../../../../UI/Footer";

class TicketListViewContent extends Component {

  render() {
    const {
      addField,
      addButton,
      defaultColDef,
      deleteButton,
      rowData,
      listView,
      total,
      onGridReady,
      onSelectionChanged,
      onSearch,
      rowDoubleClicked,
    } = this.props;
    return (
      <div className="col-md-12">
        <ListHeader
          addButton={addButton}
          addField={addField}
          deleteButton={deleteButton}
          onSearch={onSearch}
        />
        <TicketRenderer
          defaultColDef={defaultColDef}
          listView={listView}
          rowData={rowData}
          onGridReady={onGridReady}
          onSelectionChanged={onSelectionChanged}
          rowDoubleClicked={rowDoubleClicked}
        />
        <Footer total={total} />
      </div>
    );
  }
}

export default TicketListViewContent;
