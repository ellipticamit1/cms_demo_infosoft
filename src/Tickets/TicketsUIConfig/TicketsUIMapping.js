/* Routing mode */
export const routeMode = {
    new: "new",
    edit: "edit",
    versions: "versions",
};

/* MDM Api call details */
export const apiCall = {
    formViewOptionsCall: "?set=formview",
    listViewCall: "/list",
    listViewOptionsCall: "?set=listview",
    dropDownCall: "?is_active=True",
    headerCall: "?set=listview&filterColumn=True",
};

/* Notification message title*/
export const notificationTitle = {
    api: "API",
    delete: "DELETE",
    form: "FORM",
    edit: "EDIT",
    version: "VERSIONS",
    permissions: "PERMISSIONS",
    login: "LOGIN",
    approve: "APPROVE",
    reject: "REJECT",
    already_exists: "Already Exists",
    forgot_password: "Forgot Password"
};

/* Notification message */
export const notificationMessage = {
    agreement: "Please accept Terms and Conditions before submitting Form",
    formSubmitSuccess: "Data Added Successfully !",
    formSubmitError: "ERROR Inserting the data!",
    fileSizeExceeded: "File size is too large!",
    fileLimitExceeded: "Maximum limit exceeded for uploading files",
    fileTypeNotAllowed: "Uploaded type of file is not allowed",
    formUpdateSuccess: "Data Updated Successfully for ID: ",
    formUpdateError: "ERROR Updating the data! Check",
    listDeleteSuccess: "Data Deleted Successfully",
    listDeleteError: "ERROR Deleting the data! Check",
    listMultipleDeleteSuccess: "Multiple Data Deleted Successfully",
    listDeleteDataNotSelected: "Please select rows to delete",
    listEditDataNotSelected: "Please select rows to edit",
    listMultipleEditDataSelected: "Only single row can be edited",
    apiConnectionError:
        "Internal Server Error, Please contact our team on email@email.com ",
    versionCheckDataNotSelected: "Please select row data to check Versions !",
    versionCheckMultipleDataSelected:
        "Only single row data Versions can be Check !",
    nopermissions: "Access Denied, You do not have sufficient permissions",
    approve: "Request Approve Successfully",
    reject: "Request Reject Successfully",
    remarks: "Please enter remarks before Approve/Reject",
    userDetailFetchError: "Could not fetch users Email Id & Contact No",
    already_exists: "Object with this Value Already Exists, Kindly restore it, by contacting Admin."
};

/* Notification message type */
export const showNotificationType = {
    error: "error",
    success: "success",
    warning: "warning",
    info: "info",
};