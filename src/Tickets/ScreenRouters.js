import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import {routeUrl} from '../App/AppConfig';
import TicketsListView from './Screens/TicketsListView';
import TicketsFormView from './Screens/TicketsFormView';

const pathUrl = `${routeUrl.url  }/`;
const screen = "Tickets";

class ScreenRouters extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route
            replace
            path={`${pathUrl}${screen}/new`}
            component={TicketsFormView}
          />
          <Route exact path={pathUrl + screen} component={TicketsListView} />
        </Switch>
      </Router>
    );
  }
}

export default ScreenRouters;
