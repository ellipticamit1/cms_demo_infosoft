from django.db import models
from reversion import revisions as reversion
from api.alias import AliasField
from api.models import BaseModel


class TicketType(BaseModel):
    # Field name made lowercase.
    ticket_type_id = models.AutoField(
        db_column='TICKET_TYPE_ID', primary_key=True)
    # Field name made lowercase.
    ticket_type = models.CharField(db_column='TICKET_TYPE', max_length=15)
    name = AliasField(db_column='TICKET_TYPE', blank=True, null=True)

    def __str__(self):
        return self.ticket_type

    class UI_Meta:
        ui_specs = {
            "listview": [
                "this value"
            ],

            "formview": [
                {
                    "sectionlabel": "Ticket Type Master",
                    "cols": 2,
                    "colComponent": [
                        {
                            "label": "Ticket Type",
                            "decorator": "ticket_type",
                            "type": "textbox",
                            "required": "true",
                            "message": "Enter Ticket Type",
                            "id": "ticket_type",
                            "placeholder": "Please Enter Ticket Type",
                            "maxlength": 3,
                            "disabled": False,
                        },
                        {
                            "label": "Is Active",
                            "decorator": "is_active",
                            "type": "radio",
                            "radioType": "group",
                            "id": "isActive",
                            "required": "true",
                            "message": "Please select the status",
                            "listed": "yes",
                            "list_data": [
                                {
                                    "True": "Yes"
                                },
                                {
                                    "False": "No"
                                },
                            ],
                            "link_api": "isactive",
                            "disabled": False,
                        },
                        {
                            "label": "Is Deleted",
                            "decorator": "is_deleted",
                            "type": "radio",
                            "radioType": "group",
                            "id": "isDeleted",
                            "required": "true",
                            "message": "Please select the status",
                            "listed": "yes",
                            "list_data": [
                                {
                                    "True": "True"
                                },
                                {
                                    "False": "False"
                                },
                            ],
                            "link_api": "isdeleted"
                        },
                        {
                            "label": "Created By",
                            "decorator": "created_by",
                            "type": "textbox",
                            "required": "true",
                            "message": "Created By !",
                            "id": "created_by",
                            "placeholder": "This is Created By :",
                            "disabled": True
                        },
                        {
                            "label": "Created Date ",
                            "decorator": "created_date",
                            "required": "true",
                            "message": "Created Date ",
                            "placeholder": "Select Date",
                            "type": "date",
                            "id": "created_date",
                            "dateFormatList": "DD-MM-YYYY HH:mm:ss",
                            "disabled": True
                        },
                        {
                            "label": "Updated By ",
                            "decorator": "last_updated_by",
                            "type": "textbox",
                            "required": "true",
                            "message": "Last Updated By !",
                            "id": "last_updated_by",
                            "placeholder": "This was last Updated By:",
                            "disabled": True
                        },
                        {
                            "label": "Last Updated : ",
                            "decorator": "last_updated_date",
                            "required": "true",
                            "message": "Last Updated Date",
                            "placeholder": "Select Date",
                            "type": "date",
                            "id": "last_updated_date",
                            "dateFormatList": "DD-MM-YYYY HH:mm:ss",
                            "readonly": "true",
                            "disabled": True
                        },
                    ]
                }

            ]
        }

    class Meta:
        db_table = 'MST_TICKET_TYPE'
        app_label = "masters"


reversion.register(TicketType)
