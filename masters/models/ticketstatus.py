from django.db import models
from reversion import revisions as reversion
from api.alias import AliasField
from api.models import BaseModel


class TicketStatus(BaseModel):
    # Field name made lowercase.
    ticket_status_id = models.AutoField(
        db_column='TICKET_STATUS_ID', primary_key=True, blank=True)
    # Field name made lowercase.
    ticket_status = models.CharField(db_column='TICKET_STATUS', max_length=15)
    name = AliasField(db_column='TICKET_STATUS', blank=True, null=True)

    def __str__(self):
        return self.ticket_status

    class UI_Meta:
        ui_specs = {
            "listview": [
                "this value"
            ],

            "formview": [
                {
                    "sectionlabel": "Ticket Master",
                    "cols": 2,
                    "colComponent": [
                        {
                            "label": "Ticket Status",
                            "decorator": "ticket_status",
                            "type": "textbox",
                            "required": "true",
                            "message": "Enter Ticket Status",
                            "id": "ticket_status",
                            "placeholder": "Please Enter Ticket Status",
                            "maxlength": 3,
                            "disabled": False,
                        },
                        {
                            "label": "Is Active",
                            "decorator": "is_active",
                            "type": "radio",
                            "radioType": "group",
                            "id": "isActive",
                            "required": "true",
                            "message": "Please select the status",
                            "listed": "yes",
                            "list_data": [
                                {
                                    "True": "Yes"
                                },
                                {
                                    "False": "No"
                                },
                            ],
                            "link_api": "isactive",
                            "disabled": False,
                        },
                        {
                            "label": "Created By",
                            "decorator": "created_by",
                            "type": "textbox",
                            "required": "true",
                            "message": "Created By !",
                            "id": "created_by",
                            "placeholder": "This is Created By :",
                            "disabled": True
                        },
                        {
                            "label": "Created Date ",
                            "decorator": "created_date",
                            "required": "true",
                            "message": "Created Date ",
                            "placeholder": "Select Date",
                            "type": "date",
                            "id": "created_date",
                            "dateFormatList": "DD-MM-YYYY HH:mm:ss",
                            "disabled": True
                        },
                        {
                            "label": "Updated By ",
                            "decorator": "last_updated_by",
                            "type": "textbox",
                            "required": "true",
                            "message": "Last Updated By !",
                            "id": "last_updated_by",
                            "placeholder": "This was last Updated By:",
                            "disabled": True
                        },
                        {
                            "label": "Last Updated : ",
                            "decorator": "last_updated_date",
                            "required": "true",
                            "message": "Last Updated Date",
                            "placeholder": "Select Date",
                            "type": "date",
                            "id": "last_updated_date",
                            "dateFormatList": "DD-MM-YYYY HH:mm:ss",
                            "readonly": "true",
                            "disabled": True
                        },
                    ]
                }

            ]
        }

    class Meta:
        db_table = 'MST_TICKET_STATUS'
        app_label = "masters"


reversion.register(TicketStatus)
