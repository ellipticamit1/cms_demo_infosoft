from django.contrib import admin
from masters.models.ticketstatus import TicketStatus
from masters.models.tickettype import TicketType

# Register your models here.
admin.site.register(TicketStatus)
admin.site.register(TicketType)
